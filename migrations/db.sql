/**
 * Author:  Aleksey Shutiy<a.shutiy@gmail.com>
 * Created: 18.02.2017
 */
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `family` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(32) NOT NULL,
  `password` char(255) NOT NULL,
  `birthday` date NOT NULL,
  `status` int(1) NOT NULL,
  `photo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `idx_email` (`email`),
  KEY `idx_date` (`birthday`)
);

CREATE TABLE IF NOT EXISTS `works` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `company` varchar(255) NOT NULL,
  `position` varchar(255) NOT NULL,
  `w_start_date` date NOT NULL,
  `w_end_date` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_user` (`user_id`),
  KEY `idx_start_date` (`w_start_date`),
  KEY `idx_end_date` (`w_end_date`)
);

CREATE TABLE IF NOT EXISTS `education` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `institut` varchar(255) NOT NULL,
  `faculty` varchar(255) NOT NULL,
  `e_start_date` date NOT NULL,
  `e_end_date` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_user` (`user_id`),
  KEY `idx_start_date` (`e_start_date`),
  KEY `idx_end_date` (`e_end_date`)
);