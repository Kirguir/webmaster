<?php
use work\model\Lang;

?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Account page</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<script type="text/javascript" src="assets/js/script.js"></script>
	</head>
	<body>
		<nav class="navbar navbar-default">
			<div class="container">
				<ul class="nav navbar-nav navbar-right">
					<?php if(!$user) { ?>
					<li>
						<a href="index.php?action=reg"><?= Lang::t('register') ?></a>
					</li>
					<li>
						<a href="index.php?action=login"><?= Lang::t('login') ?></a>
					</li>
					<?php } else { ?>
					<li>
						<a href="index.php?action=logout"><?= Lang::t('logout') ?></a>
					</li>
					<?php } ?>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?= Lang::t('language') ?> (<?= $language ?>) <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="index.php?action=switch&lang=ru"><?= Lang::t('russian') ?></a></li>
							<li><a href="index.php?action=switch&lang=en"><?= Lang::t('english') ?></a></li>
						</ul>
					</li>
				</ul>
			</div>
		</nav>
		<?php if($alert) {
			$alert = Lang::t($alert);
			echo "<div class='alert alert-danger' role='alert'>{$alert}</div>";
		} ?>
		<?php include $view ?>
	</body>
</html>